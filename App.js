import React, {Component} from 'react';
import { StyleSheet, Text, View, TextInput,
         TouchableOpacity, FlatList, Switch } from 'react-native';

export default class App extends Component {
  state = { 
    filterValue: "",
    switchValue: false,
    description: "",
    toDoItem: [],
  };
  
  onAddItem = async () =>{
    const arrayIndex = this.state.toDoItem.length;
    this.setState(() => ({
      toDoItem:[...this.state.toDoItem,{
        taskId: arrayIndex,
        description: this.state.description,
        switchValue: false
      }],
    }));

    this.setState(() => ({
      description: "",
    }));
  }
  
  toggleSwitch = (item) =>{
    const newArray = this.state.toDoItem.map((toDoCopy) => {
      if (toDoCopy.taskId === item.taskId) {
        return ({
          description: toDoCopy.description,
          switchValue: !toDoCopy.switchValue,
          taskId: toDoCopy.taskId,
        });
      }
      return toDoCopy;
    });
    this.setState({ toDoItem: newArray });
  };

  _renderItem = ({item}) => (
    <View style={styles.toDoGrid}>
      <Switch 
        onValueChange={() => this.toggleSwitch(item)} 
        value={item.switchValue}>
      </Switch>
      <Text>
        {item.description}
      </Text>
    </View>
  );
  
  filterAll = () => {
    this.setState({filterValue: ""})
  }
  filterCompleted = () => {
    this.setState({filterValue: "completed"})
  }
  filterNotCompleted = () => {
    this.setState({filterValue: "not completed"})
  }

  render() {
    var filterItem;
    if(this.state.filterValue == "completed"){
       filterItem = this.state.toDoItem.filter( item => item.switchValue == true);
    }else if (this.state.filterValue == "not completed"){
       filterItem = this.state.toDoItem.filter( item => item.switchValue == false);
    } else {
       filterItem = this.state.toDoItem;
    }
    
    return (
      <View style={styles.main}>
        <View style={styles.header}>
          <Text style={styles.title}>To Do List</Text>
        </View>

        <View style={styles.newTaskContainer}>
          <TextInput 
            style={styles.titleBox} 
            placeholder="Write your to do item here"
            placeholderTextColor="#000000"
            underlineColorAndroid="grey"
            onChangeText={(description) => this.setState({description: description})}
            value={this.state.description}
          />
          
          <TouchableOpacity
          onPress={this.onAddItem}
          >
            <View style={styles.addButton}>
              <Text style={styles.addText}>
                ADD
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.listView}>
          <FlatList
            data = {filterItem}
            keyExtractor = {item => item.taskId.toString()}
            renderItem = {this._renderItem}
          />
        </View>

        <View style={styles.displayButtonGrid}>
          <TouchableOpacity onPress={this.filterAll}>
            <View style={styles.selectAllButton}>
              <Text style={styles.selectAllText}>ALL</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={this.filterCompleted}>
            <View style={styles.selectCompleteButton}>
              <Text style={styles.selectCompleteText}>COMPLETED</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={this.filterNotCompleted}>
            <View style={styles.selectNotCompleteButton}>
              <Text style={styles.selectNotCompleteText}>NOT COMPLETED</Text>
            </View>
          </TouchableOpacity>
        </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main:{
    flex:1,
  },
  header:{
    height: 80,
    backgroundColor: 'grey',
    flexDirection: 'row',
    alignItems: 'center',
  },
  title:{
    fontSize: 40,
    fontWeight: 'bold',
    color: 'white',
    paddingLeft: 30,
  },
  newTaskContainer:{
    height: 60,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  titleBox:{
    height: 40,
    width: 340,
  },
  addButton:{
    height: 45,
    width: 50,
    backgroundColor: 'blue',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  addText:{
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
  listView:{
    height: 450,
  },
  displayButtonGrid:{
    height: 70,
    backgroundColor: '#dddddd',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  selectAllButton:{
    height: 60,
    width: 50,
    backgroundColor: '#7c7c7c',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  selectAllText:{
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
  selectCompleteButton:{
    height: 60,
    width: 130,
    backgroundColor: '#7c7c7c',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  selectCompleteText:{
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
  selectNotCompleteButton:{
    height: 60,
    width: 180,
    backgroundColor: '#7c7c7c',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  selectNotCompleteText:{
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
  toDoGrid:{
    height: 50,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: 40,
  },
});
